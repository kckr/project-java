WordCount
如何运行

使用命令行输入参数：输入文件、输出文件

javac -encoding UTF-8 WordCount.java
java WordCount input.txt output.txt
功能简介

统计input.txt中的以下几个指标

统计文件的字符数（对应输出第一行）
统计文件的单词总数（对应输出第二行）
统计文件的有效行数（对应输出第三行）
统计文件中各单词的出现次数（对应输出接下来10行）
将统计结果输出到output.txt，输出的格式如下

characters: number
words: number
lines: number
word1: number
word2: number
...
